
/*
 *  Árvore AVL (Adelson-Velskii e Landis)
 *
 *  Renomeie o arquivo incluindo seu nome no final (trab3-avl-Tiemi.c)
 *  Coloque seu nome aqui (este trabalho é individual)
 *
 * Você não deve alterar as funções já escritas, porém poderá incluir funções auxiliares.
 */

#include <stdlib.h>
#include <stdio.h>

typedef struct no {
  int info;
  int bal;   /* hdir - hesq */
  struct no *esq, *dir,*pai;
} No;

int altura(No* t) {
  if (t == NULL)
    return 0;
  int hesq = altura(t->esq);
  int hdir = altura(t->dir);
  t->bal = hesq-hdir;
  return hesq > hdir ? hesq + 1 : hdir + 1;
}

No* cria(int chave, No* esq, No* dir, No * pai) {
  No* n = (No*) malloc (sizeof(No));
  n->info = chave;
  n->bal = altura(dir) - altura(esq);
  n->esq = esq;
  n->dir = dir;
  if(pai == NULL)
    n->pai = n;
  else
    n->pai = pai;
  return n;
}

void atualizaPais(No * p, No * pais){
  if(p == NULL){
    return;
  }else{
    p->pai = pais;
    atualizaPais((p)->esq,p);
    atualizaPais((p)->dir,p);
  }
}

void varreduraManual(No *p){
  int input;
  int variavel = 1;
  int hue;
  system("clear");
  do{
    printf("0 - Voltar\n");
    printf("1 - Esquerda\n");
    printf("2 - Direita\n");
    printf("--------------------------\n");
    printf("CHAVE ATUAL: %d\n",p->info);
    if(p->esq != NULL)
      printf("ESQ: %d\n",p->esq->info);
    else
      printf("ESQ: VAZIA\n");
    if(p->dir != NULL)
      printf("DIR: %d\n",p->dir->info);
    else
      printf("DIR: VAZIA\n");

    if((p->pai != p)&&(p->pai != NULL)){
      printf("PAI: %d\n",p->pai->info);
    }

    printf("ESCOLHA: ");
    scanf("%d",&input);

    switch(input){
      case 0:
        return;
        break;
      case 1:
        if(p->esq != NULL)
          varreduraManual(p->esq);
        else
          printf("ESQUERDA É VAZIA!\n");
        break;
      case 2:
        if(p->dir != NULL)
          varreduraManual(p->dir);
        else
          printf("DIREITA É VAZIA!\n");
        break;
      default:
        variavel = 0;
        break;
    }
    printf("APERTE ESPACO E DEPOIS ENTER PARA CONTINUAR....\n");
    scanf("%[^ ]d",&hue);
    hue = 0;
    system("clear");
  }while(variavel);
}

int busca (No * p, int chave) {
  int aux = 0;
  // caso a arvore esteja vazia
  // retorna que não foi encontrada
  if(p == NULL)
    return 0;

  // se a chave for encontrada
  // retorna 1 na recursão
  if(p->info == chave){
    return 1;
  }else{
    // caso não encontre a chave
    // manda recursivamente os dois nós
    // (esq e dir) para a busca recursiva
    aux = busca(p->esq,chave);
    if(aux == 1)
      return aux;
    return busca(p->dir,chave);
  }
}

int verifica_AVL(No* t) {

}

void preorder(No* t){
	if (t != NULL){
		printf("%d ", t->info);
		preorder(t->esq);
		preorder(t->dir);
	}
}

void inorder(No* t){
	if (t != NULL){
		inorder(t->esq);
		printf("%d ", t->info);
		inorder(t->dir);
	}
}

void postorder(No* t){
	if (t != NULL){
		postorder(t->esq);
		postorder(t->dir);
		printf("%d ", t->info);
	}
}
// **pa = &aux; &aux ==> endereço do ponteiro
//
//
//
void LL(No *f, No *p, No ** aux) {
  (*p).esq = f->dir;
  f->dir = p;

  if(p->pai == p){
    *aux = f;
  }else{
    p->pai->esq = f;
  }
}

void RR(No* f,No * p, No ** aux){
  (*p).dir = (f)->esq;
  (*f).esq = p;

  if((p)->pai == p){
      *aux = f;
  }else{
    p->pai->dir = f;
  }
}

void LR(No** r) {
  No * pa = *r;
  No * pb = pa->esq;
  No * pc = pb->dir;

  pb->dir = pc->esq;
  pc->esq = pb;
  pa->esq = pc->dir;
  pc->dir = pa;
  pa = pc;
}

void RL(No** r) {

}


/* Retorna 1 se inseriu ou 0 se 
   o elemento ẽ repetid. */
int insere(No **t, int chave) {
  No * aux2 = *t;
  No * aux = NULL;
  if(*t == NULL){
    // a arvore esta vaza.
    *t = cria(chave,NULL,NULL, *t);
    return 1;
  }else{
    // na arvore tem algo
    if(!busca(*t,chave)){
      // chave não esta repetida
      aux = *t;
      while(aux != NULL){
        if(chave > aux->info){
          if(aux->dir == NULL)
            break;
          aux2 = aux;
          aux = aux->dir;
        }else{
          if(aux->esq == NULL)
            break;
          aux2 = aux;
          aux = aux->esq;
        }
      }
      if(chave > aux->info){
        aux->dir = cria(chave, NULL, NULL, aux2);
      }else{
        aux->esq = cria(chave, NULL, NULL, aux2);
      }
      altura(*t);
      if(aux2->bal == 2){
        if(aux->bal == 1){
          printf("LL\n");
          LL(aux,aux2,t);
        }else{
          if(aux->bal == -1){
            printf("LR\n");
            LR(&aux2->pai);
          }
        }
      }else{
        if(aux2->bal == -2){
          if(aux->bal == 1){
            printf("RL\n");
          }else{
            if(aux->bal == -1){
              printf("RR\n");
              RR(aux,aux2,t);
            }
          }
        }
      }
      atualizaPais(*t,*t);
    }else{
      // chave repetida
      return 0;
  }
    }
}

void libera(No** p){
	if(*p != NULL){
		libera(&(*p)->esq);
		libera(&(*p)->dir);
		free (*p);
		*p = NULL;
	}
}

int main() {
  No *t = NULL;
  int num;

  scanf("%d", &num);
  while(num != -1){
    if(num == -123)
      varreduraManual(t);
    insere(&t, num);
    //inorder(t);
    //printf("\n");
    preorder(t);
    printf("\n");
   // postorder(t);
   // printf("\n");
    if (!verifica_AVL(t))
      printf("insercao com problema\n");
    scanf("%d", &num);
  }

  libera(&t);
  return 0;
}
